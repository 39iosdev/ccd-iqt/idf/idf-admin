## VIRTUAL ENVIRONMENT SET-UP:  

### DISCORD
* **Creating an account:**  Please use the link provided to register your account on [Discord](https://discord.gg/tStgaZ2e3d). If you do not have an account, you will be asked to provide an email and username. The username in this case, is the name everyone will see. For accountability there are certain guidelines we must follow. If you are inclined to use a pseudonym, please choose one that is appropriate and provide your real name along with it.  

    * For Military, your name must have Rank / Last Name because that is what we are required to reference you as; However, you can add a callsign/nickname. For example:  

        *	Capt. “Planet” Smith
        *   John "Bones" Jones

    * For Civilian / Contractors the same general rules apply. We must maintain a professional environment; therefore, please keep that in mind with our GO BY names.  

* **Operating Discord:** There are two methods of using Discord. You can download the app specific to your operating system or simply open Discord in your browser. It is **HIGHLY** recommended that the desktop app is utilized. If you choose to utilize the browser and you are having issues with the web browser, try refreshing the page. In most cases, that fixes the problem.  

![alt text](assets/Screenshot_Discord.png)

<p>&nbsp;</p>  

### GITLAB / PLATFORM / OPTIMUS 
:eight_spoked_asterisk:**IMPORANT**:eight_spoked_asterisk: We have moved to a new system, where our content will be hosted on Optimus, so please follow the instructions in the **Pre-IDF Student Instructions** PDF attachment provided in the email. Keep in mind the set-up instructions for Appgate is done on commercial internet, **NOT** NIPR. Unlike traditional training environments everything will be delivered virtually, including the study guides; therefore, it is imperative that you set-up your account before class starts. First ensure you have one created by visiting [Platform One Account](https://login.dso.mil/). If you dont have an account You will need to visit (https://sso-info.il2.dso.mil/new_account.html) to create one. 

Printing the study guides is of little benefit and may be cost-prohibitive as well. A secondary screen may be helpful as an alternative to printing material.  

