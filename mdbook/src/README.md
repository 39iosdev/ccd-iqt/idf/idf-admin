MEMORANDUM FOR INITIAL DEVELOPER FUNDAMENTALS STUDENTS  

FROM:  39 IOS Det 1/Initial Developer Fundamentals Course Director  

SUBJECT: IDF Attendees  

## WELCOME
As the course director, I welcome you to the 39 IOS Det 1 Initial Developer Fundamentals Course. Our goal is to create a positive experience and develop instruction that allows students to succeed here and beyond the schoolhouse. So, please take the time to read this letter as it contains essential information that will assist in completing IDF. 

After reading through this welcome letter and completing any actions requested, if you have any questions or issues, don't hesitate to get in touch with me at one of the following: [.mil email] ivan.wright@us.af.mil; [commerical email] ivanemil.wright@gmail.com; or [personal cell]210-842-0116. 

Please read the 39 IOS Det 1 [CC Welcome Letter](assets/Welcome%20Letter%20from%20CC.pdf) then continue to [Course Schedule/Attendance Requirements](sched-reqs.md).
