# Administrative Issues

## Administrative Withdrawal
At the discretion of the 39 IOS Det 1 Commander, students may be administratively withdrawn from the course. Certain actions may be accompanied by a report from the schoolhouse commander to the member’s current and/or gaining commander with a recommendation for administrative or disciplinary action. Below are examples of actions that could result in removal: 

1.  Student-Initiated Elimination (SIE)

2.  Action punishable under the Uniform Code of Military Justice (UCMJ)

3.  Maliciously altering computer files and/or tampering with unauthorized equipment

4.  Web browsing, reading email during lectures, reading of non-duty related material during lessons/exercises

5.  Failure to follow course policies regarding student responsibilities as outlined in this student handbook and as briefed during in-processing. Students will acknowledge having read and understood this handbook following the day one in-processing briefing

6.  Inappropriate behavior, behavior detrimental to class operations (to include unprofessional actions toward an instructor, or guest speaker)

7.  Excessive lateness

8.  **Improper handling of course material:** Any misuse of test material or test compromise will result in student removal from the course without graduation or course credit. For students who have already graduated, course credit may be revoked

    - Students will not knowingly possess, reproduce, distribute, or communicate in any way the contents of test material, unless specifically authorized in writing by 39 IOS Standardization & Evaluation flight

    - Possessing cheat sheets, notes or study guides produced by anyone other than your class, or handed out to you by 39 IOS staff

    - Posting or sharing any course material on the Internet (social media, Quizlet, etc.)

    - E-mailing course material

9.  **Test compromise/Reporting Compromises:** Students are prohibited from any actions which constitute test compromise. Specifically, students are prohibited from:

    -   Possessing, distributing, reviewing, copying, transmitting, having access to actual test material, or allowing/causing access to unauthorized individuals

    -   Discussing or sharing, in any form, information about actual test material with anyone

    -   Distributing any test-related notes or study-aids to anyone other than the students in your class or 39 IOS staff, to include publication on internal networks or the internet

    -   Copying, reproducing, or screen capturing test material from any 39 IOS testing system. If any student has any knowledge of another student in violation of the above provisions, the student with knowledge is required to report it to the Course Director or an instructor immediately.  

    -   Other situations deemed by commander that warrant removal

<p>&nbsp;</p>  

## Leave Policy  
Leave during class time will only be approved in emergencies, and handled on a case-by-case basis. It is highly encouraged to wait for approval prior to making any travel arrangements. NOTE: According to the Joint Travel Regulations (JTR) p. 2M-6, unless receiving long-term TDY flat rate per diem, only the lodging portion will be reimbursed during periods of authorized leave. In all cases, the weekend sign-out roster will be utilized for all travel that is taken that will result in the students not sleeping in their designated quarters reflected on the recall roster. Class leaders will conduct and document pre-departure safety briefings for all students under the age of 26 on leave, regardless if leave is taken in or out of the local area. The briefing will be documented on AF Form 4392, Pre-departure Safety Briefing. AF Form 4392s are located in the Class leader binder.

<p>&nbsp;</p>  

## Safety
Students desiring to participate in extreme sports/high-risk activities must produce proof of training, if applicable, when requesting the 39 IOS Det 1 commander’s authorization to participate in the activity. Students will not be provided 39 IOS Det 1 equipment or facilities to complete training. A high-risk activities worksheet (AF Form 4391) will be completed prior to participation in extreme sport/activity. The Class leader and the Unit Safety Representative will assist students with completing the AF Form 4391 and schedule a meeting with the 39 IOS Det 1/ CC for final approval. AF Form 4391s are located in the Class leader binder. Additional information will be provided during the safety briefing on Day 1 of training.
The use of all tobacco products are prohibited within DoD facilities. In accordance with AFI 40-102, while in uniform, students enrolled in formal training are prohibited from smoking.

### Sports and Recreation  
* Always warm up prior to physical activities
* Drink plenty of fluids before, during, and after vigorous activities
* Use the necessary personal protective equipment
* Inspect the play area and equipment for hazards
* Take time to cool down

<p>&nbsp;</p>  

### Extreme Sports-High Risk Activities  

|                                   |                               |                   |                      |
|-----------------------------------|-------------------------------|-------------------|----------------------|
|**All Terrain Vehicles**           |    **Motorcycle Riding**      |**Ocean Kayaking** |**Hot Air Ballooning**|
|**Scuba Diving**                   |**Experimental Aircraft**      |**Civilian Flight**| **Soaring**          |
|**Bungee Jumping**                 |**Paintball**                  |**Parasailing**    |**Hunting**           |
|**Civilian Helicopter**            |**Rodeo/Bull Riding**          |**Dirt Biking**    |**Surfboarding**      |
|**Jet Skiing/Personal Water Craft**|**White Water Rafting**        |**Mountain Biking**|**Base Jumping**      |  
|**Auto Racing**                    |**Mountain Climbing/Rappeling**|**Boating**        |**Skydiving**         |
           
<p>&nbsp;</p>  

### High-Risk Safety Briefing
* High-Risk Activity approval for staff members is delegated to the Operations Officer, Flight Commander, or Flight Chief.  

* High-Risk Activity approval for students is delegated to the Flight Commander, Flight Chief, or Course Director.

### Mishap Reporting
* Report details to class safety rep and class leader NLT the next duty day.
* According to **AFI 91-204 1.3.3.1. An AF Form 978 will be completed by the injured personnel's supervisor** and returned to the appropriate safety office *within five (5) workdays* following the mishap or notification of the mishap, whichever is earlier.



### Teleworking Conditions
(Does not apply to this iteration) Since this course is being executed virtually, the student is responsible for local environmental factors. Please ensure cable management, proper lighting, and adequate surge protection is in place in your physical environment. Please ensure you are stationary when listening to lectures or watching instructor demos. If there is an emergency at your location, please be sure to ensure human safety first, contact necessary emergency services, then contact the Course Director once you are in a safe place to explain the emergency situation. At that time, let the Course Director know whether or not assistance is needed.  

<p>&nbsp;</p>  

## Additional Points of Contact  
If you have any additional questions or concerns, feel free to reach out to your Course Director.  

### Course Director
-  Name: Mr. Ivan Wright Jr.
-  e-mail: ivan.wright@us.af.mil 
-  Phone (personal): 210-842-0116

### Superintendent
- Name: MSgt Francis K. Armstrong
- e-mail: francis.armstrong@us.af.mil
- DSN: 969-6158
- Commercial: 210-977-6158



Given COVID-19 and current HPCON, personnel are primarily teleworking. Please utilize e-mail as the primary method of contact. If making contact by phone, please identify yourself and the purpose of your call with a return number for the best results. Welcome to 39 IOS Det 1, and we are looking forward to meeting you and having you in class.
