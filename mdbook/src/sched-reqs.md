## COURSE SCHEDULE/ATTENDANCE REQUIREMENTS
Please review this section carefully as it contains pertinent information about the course and what we expect from our students. A link to the schedule will be provided in a separate email and here for your convenience. If you cannot open the link, please notify the CD. All times are in Central Standard Time, so keep that in mind when looking at the schedule. The class will run from **6 June – to 5 October 2022** and will execute in person. **Course location**: 3133 General Hudnell Dr # 100, San Antonio, TX 78226, room 125. We understand that life happens, so if you need to step away from class or will be missing lessons, please notify your instructor and the course director. The report time for the first day of training will be 0800 **CST** on 6 June 2022. Please ensure your [Discord](https://discord.gg/tStgaZ2e3d) access is working **before 6 June**; if you encounter issues, please reach out to your course director, [Mr. Ivan Wright](mailto:ivan.wright@us.af.mil).  

### Attendance  
Attendance is mandatory. Please respect everyone’s effort and time. Again, we understand that life happens. Suppose you know you will not be in the classroom due to doctor appointments, issues with your vehicle, or general emergencies. We ask that you notify an instructor, Class leader, or CD. We also ask that you re-schedule non-emergency-related appointments to after-class hours.   
### Tech Requirements  
At minimum, a working laptop and internet will suffice. We recommend a dual screen set-up if you have the means. If you don’t have a second monitor, using a TV is a good alternative if you're in a pinch. A quick word on the software and applications. Some of you may already have the software/applications listed below, installed on your system. For those of you that don’t, do not worry, there are sections built into the course where we will guide you through the set-up.          

  * **Minimum Hardware Requirments:**   
    - Operating system:(64-bit) 
    - Windows 10 or any linux OS version 
    - Quad-Core Processor
    - Processor: 1 gigahertz (GHz) or faster
    - RAM:  4 GB (64-bit) 
    - Free hard disk space: 250GB
   * **Software/Applications:**   
        - Python 3
        - Mingw/ Cygwin
        - Visual Studio code
        - Virtualbox/Vmwareplayer
        - Preferred editor/IDE (pycharm, codeblocks, clion)

### HOW TO REQUEST A LAPTOP
If you do not posses equipment that meets the system technical requirements please contact [Ivan Wright] ivan.wright@us.af.mil and we will provision a laptop to you. **Please keep in mind at this moment we are low in assets therefore, it is not a gurantee that we will have a laptop to supply**. Please provide the following information in your email when requesting a laptop:  
  * **Name/Rank** 
  * **Email/Phone number** 
  * **Physical address** (This is the address the laptop will be shipped to)
  * **Supervisor name/rank, email, phone number** 


### TEAM WORK
Please share your knowledge with others! Learning may feel like an individual effort and for the most part that may be true. But we become more intelligent and productive when we share that knowledge with others. Conversely, we must not be too stubborn to ask for assistance.  Students will come in at various levels of subject knowledge and those groups that share and ask questions benefit the most from this course.   

### FORMAL CLASS SESSIONS
All formal class sessions will take place in-person at the location mentioned above; therefore, be on time when coming back from breaks. We like to follow a 50-10 session rule, meaning 50 min of instruction followed by a 10 min break. With that said please be respectful, be engaged, and minimize any distractions during class. 
